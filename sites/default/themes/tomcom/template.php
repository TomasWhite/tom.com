<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728096
 */


/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
/* -- Delete this line if you want to use this function
function tomcom_preprocess_maintenance_page(&$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  tomcom_preprocess_html($variables, $hook);
  tomcom_preprocess_page($variables, $hook);
}
// */

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
/* -- Delete this line if you want to use this function
function tomcom_preprocess_html(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // The body tag's classes are controlled by the $classes_array variable. To
  // remove a class from $classes_array, use array_diff().
  //$variables['classes_array'] = array_diff($variables['classes_array'], array('class-to-remove'));
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
/* -- Delete this line if you want to use this function
function tomcom_preprocess_page(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function
function tomcom_preprocess_node(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // Optionally, run node-type-specific preprocess functions, like
  // tomcom_preprocess_node_page() or tomcom_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
}
// */

/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function tomcom_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the region templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function tomcom_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--sidebar.tpl.php template for sidebars.
  //if (strpos($variables['region'], 'sidebar_') === 0) {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('region__sidebar'));
  //}
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function tomcom_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  // $variables['classes_array'][] = 'count-' . $variables['block_id'];

  // By default, Zen will use the block--no-wrapper.tpl.php for the main
  // content. This optional bit of code undoes that:
  //if ($variables['block_html_id'] == 'block-system-main') {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('block__no_wrapper'));
  //}
}
// */




function hsr_oauth_connector(){
  global $user;
  //$html = "";
  
  if(arg(0) == 'user' || arg(0) == 'users'){
    $uid = arg(1);   
    $account = user_load($uid);
    $accountUid = $account->uid;
    $userUid = $user->uid;
    
    if ($accountUid  == $userUid) {
      $my_form = drupal_get_form('connector_button_form', $account, 'Add !title account');
      $connections = _connector_get_user_connections($account);
      
      //dpm($connections);
      
      foreach ($connections as $connection) {
        $connector = _connector_get_connectors($connection->connector);
        $connector_title = $connector['title'];        
      }
        
      $connector_t = 'oauthconnector_twitter';
      $connector_l = 'oauthconnector_linkedin';
      
      if( isset($connections) ){  
        if( isset($connections[0]) && !isset($connections[1]) ){
          if($connector_title == 'Twitter'){
            $cid = $connections[0]->cid;
            print "<p>You're connected with Twitter</p>";
            print "<a href='/user/". $userUid . '/connections/' . $connector_t . '__' . $cid . '/delete' . "'>Remove connection</a>";
            print "<p>Add Linked In to your account?</p>";
            
            $my_form = drupal_get_form('connector_button_form', $account, 'Add !title account');
            unset($my_form['oauthconnector_twitter']);            
            print drupal_render($my_form);

          } elseif ($connector_title == 'LinkedIn'){
            $cid = $connections[0]->cid;
            print "<p>You're connected with LinkedIn</p>";
            print "<a href='/user/". $userUid . '/connections/' . $connector_l . '__' . $cid . '/delete' . "'>Remove connection</a>";
            print "<p>Add Twitter to your account?</p>";
            
            $my_form = drupal_get_form('connector_button_form', $account, 'Add !title account');
            unset($my_form['oauthconnector_linkedin']);
            print drupal_render($my_form);
          } 
        } elseif( isset($connections[1]) ){
          $cid_t = $connections[0]->cid;
          $cid_l = $connections[1]->cid;
          print "<p>You have both Twitter and LinkedIn connected</p>";
          print "<a href='/user/". $userUid . '/connections/' . $connector_t . '__' . $cid_t . '/delete' . "'>Remove Twitter</a></br>";
          print "<a href='/user/". $userUid . '/connections/' . $connector_l . '__' . $cid_l . '/delete' . "'>Remove LinkedIn</a>";
        } 
      }
      
      if(!$connections) {
        print "<p>Add Twitter or LinkedIn to your account?</p>";

        $my_form = drupal_get_form('connector_button_form', $account, 'Add !title account');
        print drupal_render($my_form);
      }      
    }        
  }
}

