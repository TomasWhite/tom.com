/*
Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

/*
 * This file is used/requested by the 'Styles' button.
 * The 'Styles' button is not enabled by default in DrupalFull and DrupalFiltered toolbars.
 */
 
if(typeof(CKEDITOR) !== 'undefined') {
    CKEDITOR.addStylesSet( 'drupal',
    [
	{ name : "Paragraph", element : "p", attributes : { 'class' : "p-incontent" } },
	/*{ name : "Para Clear", element : "p", attributes : { 'class' : "p-clearing" } },*/
	{ name : "Highlight", element : "p", attributes : { 'class' : "highlight" } },
	{ name : "Intro", element : "p", attributes : { 'class' : "p-intro" } },
    /*{ name : "Caption", element : "p", attributes : { 'class' : "caption" } },*/
	/*{ name : "Note/Foot", element : "p", attributes : { 'class' : "note" } },*/
	{ name : "Pull Quote", element : "p", attributes : { 'class' : "pull pull_quote" } },
    /*{ name : "Quote Caption", element : "p", attributes : { 'class' : "pull pull_quote_cap" } },*/
    /*{ name : "Pull Qu Ref", element : "p", attributes : { 'class' : "pull pull_ref" } },*/
	/*{ name : "Big Letter", element : "p", attributes : { 'class' : "big-cap" } },*/

	{ name : "Heading A", element : "h3", attributes : { 'class' : "h3-content" } },
	{ name : "Heading B", element : "h4", attributes : { 'class' : "h4-content" } },
	{ name : "Heading C", element : "h5", attributes : { 'class' : "h5-content" } },
	/*{ name : "Pull Heading", element : "h6", attributes : { 'class' : "h5-pull" } },*/

	{ name : "Arrow Link", element : "a", attributes : { 'class' : "arrow-link" } },
    /*{ name : "Link", element : "a", attributes : { 'class' : "inline-link" } },*/
      
	//{ name : "[1] Ref No", element : "sup", attributes : { 'class' : "ref-no" } },
	//{ name : "[1] Ref text", element : "p", attributes : { 'class' : "ref-text" } },


	/*{ name : "Cap Img 150px", element : "img", attributes : { 'class' : "imgsmall" } },*/
	/*{ name : "Cap Img 280px", element : "img", attributes : { 'class' : "imgmedium" } },
	{ name : "Cap Img 600px", element : "img", attributes : { 'class' : "imglarge" } },*/
	
	/*{ name : "Intro Image", element : "span", attributes : { 'class' : "imgintro" } },*/
	/*{ name : "Image Clear", element : "img", attributes : { 'class' : "imgclear" } },*/
	{ name : "Image Centre", element : "img", attributes : { 'class' : "imgcentre" } },
	{ name : "Image Left", element : "img", attributes : { 'class' : "imgleft" } },
    { name : "Image Left Clear", element : "img", attributes : { 'class' : "imgleftclear" } },
	{ name : "Image Right", element : "img", attributes : { 'class' : "imgright" } },
    { name : "Image Right Clear", element : "img", attributes : { 'class' : "imgrightclear" } }

]);
}