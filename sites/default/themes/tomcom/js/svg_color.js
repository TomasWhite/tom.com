$ = jQuery;

$( document ).ready(function() {
  /**
  *** Examples ***
  * For SVGs with one color
  * change_svg_color_to($('#comments'), '#770000');
  *  
  * For SVGs with multiple colors, choose color to replace and new color
  * change_svg_color_from_to( $('#comments'),'#F0027E', '#68A4E8' );
  *
  * Reset SVG with one color to original color, or use optional color arg to reset specified color to orig color
  * reset_svg_color_to( $('#page-title'), '#68A4E8' );
  *
  * Reset SVG to original img 
  * revert_to_original_img( $('#comments') );
  */ 
  
  
});



/**
 * Detects all elements with background-image
 * @param {string} the element to be targeted
 * @return {class} class for elements with background-image
 */
function tag_all_svg_images(target){
 
  $(target).filter(function() {
    if (this.currentStyle) 
              return this.currentStyle['backgroundImage'] !== 'none';
    else if (window.getComputedStyle)
              return document.defaultView.getComputedStyle(this,null)
                             .getPropertyValue('background-image') !== 'none';
  }).addClass('bg_found');

  test_bg_image('.bg_found');
}


/**
 * Detects type of image at target
 * @param {string} the element to be targeted
 * @return {class} class shows what type of background-image is found at target
 */
function test_bg_image(target){

  if( target.length == 1 ){
    if( target.hasClass("bg_tested") ){
      return target.css("background-image"); 
    }
  }
  
  $(target).each(function(){
    
      var bg_img = $(this).css("background-image"),
        pos_svg = bg_img.indexOf(".svg"),
        pos_data = bg_img.indexOf("data:");
      
      if(pos_data != -1){
        $(this).addClass("bg_data"); 
      } else if(pos_svg != -1){
        $(this).addClass("bg_svg"); 
      } else {
        $(this).addClass("bg_only_img"); 
      }
      
      $(this).addClass("bg_tested"); 
      
  });    

  if( $(target).length == 1 ){
    return $(target).css("background-image");
  }
}

/**
 * Changes hex color for SVG made with only one color
 * @param {string} the element to be targeted 
 * @param {string} hex color
 * @return {string} SVG with new hex color {string}
 */
function change_svg_color_to(target, to_color){
  
  target.addClass("start_change_svg_color_to");
  
  //tjw to finish if more than one string is passed into function, treat each seperate
  if(target.length > 1){
    $(target).each(function(){
      change_svg_color_to(target, to_color);
    });
    return true;
  }

  if( typeof target.data("orig_color") === "undefined" ){
     orig_color = set_orig_color(target); 
  } else {
     orig_color = target.data("orig_color");
  }  
  
  if( target.hasClass('bg_data') ){
    target.addClass("start_change_svg_color_to_a");    
  
    orig_color_0 = color_check(orig_color[0], 'data');
    //console.log("orig_color_0 is "+orig_color_0);
    to_color = color_check(to_color, 'data');

    var bg = target.css("background-image"),
        color_change_img = bg.replace(orig_color_0,to_color);
    
    target.addClass("color_changed");
    target.css("background-image",color_change_img);
    
    return target.data("new_color", to_color);
    return target.data("reset_color", orig_color_0);
    
  } else if( $(target).hasClass('bg_svg') ){
    
    target.addClass("start_change_svg_color_to_b");

    to_color = color_check(to_color, 'css');
    target.css("fill",to_color);  
  } else {
    
    //target.addClass("start_change_svg_color_to_c");
    //target.addClass("error_change_svg_color_to");
  }   
}

/**
 * Resets hex color to original color for SVG made with only one color
 * @param {string} the element to be targeted 
 * @param {string} (optional) hex color. CAUTION: only resets last color to have been changed. To reset all colors use revert_to_original_img();
 * @return {string} SVG with original hex color {string}
 */
function reset_svg_color_to(target, what_color){
  if(target.length > 1){
    $(target).each(function(){
      reset_svg_color_to(target, what_color);
    });
    return true;
  }
  
  if( typeof target.data("orig_color") === "undefined" ){    
    set_orig_color(target); 
    return true; // assume no change as original color is no set
  }   
    
  if(what_color){
    new_color = target.data("new_color");
    what_color = color_check(what_color, 'data');

    if(what_color == new_color){
      reset_color = target.data("reset_color");
      change_svg_color_from_to(target, new_color, reset_color);    
      //target.addClass("color_reset");
    } else {
      revert_to_original_img(target);
    }    
    target.removeData("new_color"); 
    
  } else if( typeof what_color === "undefined" ){
    revert_to_original_img(target); 
  } 
}
/**
 * Changes hex color for SVG made with multiple colors
 * @param {string} the element to be targeted 
 * @param {string} hex color to change
 * @param {string} hex color to replace with
 * @return {string} SVG with new hex color {string}
 */
function change_svg_color_from_to(target, afrom_color, to_color){
  if(target.length > 1){
    $(target).each(function(){
      change_svg_color_from_to(target, afrom_color, to_color);
    });
    return true;
  }
  
  if( typeof target.data("orig_color") === "undefined" ){    
    set_orig_color(target); 
  } else {    
    orig_color = target.data("orig_color");
  }
  if(afrom_color == to_color){
    return true;
  }
  
  if( target.hasClass('bg_data') ){
    
    target.addClass("start_change_svg_color_to_a");
    
    afrom_color = color_check(afrom_color, 'data');
    target.data("reset_color", afrom_color);
    afrom_regexp = new RegExp(afrom_color,'g');
    
    to_color = color_check(to_color, 'data');
    target.data("new_color", to_color);
    //console.log("to_color is "+to_color);
   
    var bg = target.css("background-image"),
        color_change_img = bg.replace(afrom_regexp,to_color);
    
    target.css("background-image",color_change_img);     
    return target.data("reset_color", afrom_color);
    return target.data("new_color", to_color);
        
  } else if( $(target).hasClass('bg_svg') ){
    
    target.addClass("start_change_svg_color_to_b");

    to_color = color_check(to_color, 'css');
    target.css("fill",to_color);  
  } else {    
    //target.addClass("start_change_svg_color_to_c");
    //target.addClass("error_change_svg_color_to");
  }
}

/**
 * Resets SVG to original state
 * @param {string} the element to be targeted
 * @return {string} orginal SVG string {string}
 */
function revert_to_original_img(target){    
  orig_img = target.data("orig_img");
  target.css("background-image",orig_img);  
}

/**
 * stores original SVG and colors
 * @param {string} the element to be targeted
 * @return {array} all hex colors in SVG as {array}
 * @return records indexed hex colors of original colors in array
 */
function set_orig_color(target){
  
  var my_bg = test_bg_image(target),  
      origColor = my_bg.match( /%23([A-Fa-f0-9]{6})/g); 
  
  target.data("orig_img", my_bg);
  
  if(origColor == null){     
    origColor = target.css('fill');    
    if(origColor){      
      target.data("orig_color", origColor);      
    } else {      
      target.data("orig_color", '#222222');
      target.addClass('no_orig_color');      
    }
  } else {     
    var i,
        l = origColor.length;    
    for( i = 0 ; i < l ; i++ ){            
      origColor[i] = color_check(origColor[i], "css");
      target.data("orig_color", origColor);      
    }    
  }      
    //console.log("origColors in the array are: "+origColor);
    return target.data("orig_color", origColor[i]);
    return target.data("orig_img", my_bg);
}

/**
 * Checks correct image data is used and corrects hex length if needed
 * @param {string} color to be passed
 * @param {string} format of the color, data or hex
 * @return {string} 6 digit hex as either data or hex {string}
 */
function color_check(a_color, format){  
  
  a_color = decodeURIComponent(a_color);
  a_color = a_color.substring(0,7);
  
  if( a_color.indexOf('%') != -1 ){
    a_color = a_color.replace("%23","#");
    a_color = a_color.substring(0,7);
  }  
  
  //format as standard #NNNNNN
  if(a_color.length == 7){
    
  } else if(a_color.length == 6){
    a_color = "#"+a_color;
  } else if(a_color.length == 4){
    a_color = "#"+a_color.substring(2,1)+a_color.substring(2,1)+a_color.substring(3,1)+a_color.substring(3,1)+a_color.substring(4,1)+a_color.substring(4,1);
  } else if(a_color.length == 3){
    a_color = "#"+a_color.substring(1,1)+a_color.substring(1,1)+a_color.substring(2,1)+a_color.substring(2,1)+a_color.substring(3,1)+a_color.substring(3,1);
  }
  
  if(a_color.length != 7){
     a_color = "#111111";
  }
  
  //trim if needed
  if(format == 'data'){
    a_color = a_color.substring(1, 7);
    a_color = '%23'+a_color;
  } /*else if(format == 'css'){
    
  }*/  
  return a_color;
}


