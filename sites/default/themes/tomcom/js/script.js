/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
$ = jQuery;
(function ($, Drupal, window, document, undefined) {


// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.my_custom_behavior = {
  attach: function(context, settings) {

    
    
  }
};


})(jQuery, Drupal, this, this.document);



$( document ).ready(function() {
  google_map_field_load_map();
  
  fix_li();  
  wrap_imgs_with_captions(); 
  quote_box_with_img();
  //of_infowindow_style_fix();
  //initMap();
  //handleLocationError(browserHasGeolocation, infoWindow, pos);
  //area_finder_ajax_load();
  //area_finder_user();
  //area_finder_places();
  //area_finder_distance();
  //make_links( $('#block-system-user-menu') );
  
});

function google_map_field_load_map() {
  if ( $(".google-map-field").length){
    //$(".google_map_field_display").each(function(index, item) {
      google_map_update_style();
    //});
  }
}

function google_map_update_style(){
  // Get the settings for the map from the Drupal.settings object.
  
  var lat = $('.google_map_field_display').attr('data-lat');
  var lon = $('.google_map_field_display').attr('data-lng');
  var zoom = parseInt($('.google_map_field_display').attr('data-zoom'));
  
  // Create the map coords and map options.
  var latlng = new google.maps.LatLng(lat, lon);
  
  var json_style = [
    {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#333333"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ccffff"
            },
            {
                "saturation": "-71"
            },
            {
                "lightness": "32"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dedede"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f2f2f2"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e9e9e9"
            },
            {
                "lightness": 17
            }
        ]
    }
]
  
  var mapId = document.getElementById('map_container');
  
  var mapOptions = {
    scrollwheel: false,
    zoom: zoom,
    center: latlng,
    streetViewControl: false,
    mapTypeControl: false,
    panControl: false,
    zoomControl: true,
    styles: json_style
  };
  var map = new google.maps.Map(mapId, mapOptions);
}

function fix_li(){
 
 $('.node ol').not('.fixed, .node ol ol, .node ul ol').each(function(){
    $(this).addClass('fixed');
    $(this).find('li').not('li li').each(function(){
      $(this).addClass('fixed');
      
      $(this).wrapInner( "<span class='li_fixed'>");
      
      /*
      $(this).find('*').wrapAll('<span class="li_fixed">');
      if( !$(this).find('.li_fixed').length ){
        $( ".inner" ).wrapInner( "<span class="li_fixed">");
      }
      */
      
    });
 });
 
  
 
}

function wrap_imgs_with_captions(){
  /* fix user uploaded imgs */
   $("article img").each(function( index ) {
       /* extract alt */
       var fl_dir = ($(this).css("float"));
       
       if ($(this).attr("title")) {
           //wrap img in div, with p
           //get float from the image 
           //var fl_dir = ($(this).css("float"));
           var img_width = ($(this).css("width"));
           
           var caption = "<p class='j_caption'>" + $(this).attr("title") + "</p>";
           $(this).wrap("<div class='wrapped_img wrapped_img_" + fl_dir + "'></div>");
           
           /*
           $('.wrapped_img').append(caption);

           $('.wrapped_img').css({
             'width' : img_width,
             'float' : fl_dir,
           });
          */

           $(this).parent('.wrapped_img').append(caption);
           
           $(this).parent('.wrapped_img').css({
             'width' : img_width,
             'float' : fl_dir,
           });
       } else {
         
          $(this).addClass("non-cap");
          
          /*var message = "Hi "+fl_dir + " ";
          message = message + $(this).attr("src") + " ";
          message = message + $(this).css("margin") + " ";
          message = message + $(this).css("margin-left") + " ";
          message = message + $(this).css("margin-bottom") + " ";*/
          
          if($(this).css("margin") == "0px"){
            $(this).addClass("recheck-margin");
            $(this).css("margin-bottom", "15px") ;
            if(fl_dir == 'right'){
              $(this).css("margin-left", "15px") ;
            } else {
              $(this).css("margin-right", "15px") ;
            }
          } else {
            $(this).addClass("margin-notzero");
          }
         //alert(message);
          
         
       }
   });
  
}

function quote_box_with_img(){   
  
  
  $(".pull_quote").each(function(){
    $(this).nextAll(".wrapped_img").andSelf().wrapAll('<div class ="quote_wrapper" />');
  });
    
}

function of_infowindow_style_fix(){
  /*this function doesn't work -> see ip_geoloc_infowindow_fix module for working version */
  var map = document.getElementById('ip-geoloc-map-of-view-open_futures_map_testing-block');
  
  if(map.length){
    var infowindow = new google.maps.InfoWindow();
    /*
    * The google.maps.event.addListener() event waits for
    * the creation of the infowindow HTML structure 'domready'
    * and before the opening of the infowindow defined styles
    * are applied.
    */
    console.log(infoPopUp);
    google.maps.event.addListener(infoPopUp, 'domready', function() {
      // Reference to the DIV which receives the contents of the infowindow using jQuery
      var iwOuter = $('.gm-style-iw');

      /* The DIV we want to change is above the .gm-style-iw DIV.
      * So, we use jQuery and create a iwBackground variable,
      * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
      */
      var iwBackground = iwOuter.prev();

      // Remove the background shadow DIV
      iwBackground.children(':nth-child(2)').css({'display' : 'none'});

      // Remove the white background DIV
      iwBackground.children(':nth-child(4)').css({'display' : 'none'});

    });
  }
}


function initMap(){  
  //$('#map').addClass('found');
  
  var mapCanvas = document.getElementById('gmap_multipin');
  
  var lat = 51.5297839;//Drupal.settings.gmfe_node_display[map_id]['mid']['lat'];
  var lon = -0.05476829999;//Drupal.settings.gmfe_node_display[map_id]['mid']['lon'];
  var zoom = 15;//parseInt(Drupal.settings.gmfe_node_display[map_id]['mid']['zoom']);
	
  // Create the map coords and map options.
  var latlng = new google.maps.LatLng(lat, lon);
  var mapOptions = {
    scrollwheel: false,
    zoom: zoom,
    center: latlng,
    streetViewControl: false,
    mapTypeControl: false,
    panControl: false,
    zoomControl: true,
    scaleControl: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles:[
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "color": "#3f484a"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "administrative.neighborhood",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "administrative.neighborhood",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "color": "#3f484a"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "weight": "1.00"
            },
            {
                "color": "#ff0000"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#d9e5ed"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "color": "#cbdce7"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "hue": "#0053ff"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "saturation": "100"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#afcadb"
            }
        ]
    },    
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#d3e1ea"
            },
            {
                "gamma": "0.48"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "hue": "#0053ff"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    }
]
  };  
  var locations = [
    ['close 4', 51.5281386, -0.061845500000004],
    ['close 3', 51.522061, -0.075374300000021],
    ['close 2', 51.5258012, -0.052658899999983],
    ['far', 51.5019266, -0.17833470000005],
    ['close', 51.5297839, -0.054768299999978]
  ];  
  
  var get_locations = get_my_locations();
  
  
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var arrMarkers = [];
  
  //$url_image = url('sites/default/files/'.file_uri_target($uri), array('absolute'=>true));
  
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      userlat = position.coords.latitude,
      userlng = position.coords.longitude;
      
      var user_marker = new google.maps.Marker({
          map: map,
          position: new google.maps.LatLng(userlat, userlng),
          icon: '/sites/default/themes/tomcom/images/map_pin/bf_user_pin.png'
      }); 
    });       
  }    
  
  var i;
  
  for (i = 0; i < locations.length; i++) {
    var pos = new google.maps.LatLng(locations[i][1], locations[i][2]);
    var title = locations[i][0];
    var infocontent = '<div class="phoneytext"><h2>' + locations[i][0] + '</h2><p>some text is being put here <a href="http://tom.com.testing.effusion.dh.bytemark.co.uk/" target="_blank">para link</a></p><a href="http://tom.com.testing.effusion.dh.bytemark.co.uk/" target="_blank">2nd link after para</a></div>';
    var new_marker = addMarker(pos, title);
    arrMarkers.push(new_marker);    
  }
  
  function addMarker (pos, title) {
    var marker = new google.maps.Marker({
        map: map,
        position: pos,
        zIndex: google.maps.Marker.MAX_ZINDEX + 1, //to put places in front of user pin as was preventing the place to te clicked when user was close to location
        icon: '/sites/default/themes/tomcom/images/map_pin/bf_place_pin.png'
        
    });        
    
    var infoBubble = new InfoBubble({
      //http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobubble/examples/example.html/
        content:infocontent,
        shadowStyle: 0,
        padding: 12,
        borderRadius: 0,
        arrowSize: 7,
        borderWidth: 1,
        borderColor: '#d7d7d7', //'#0ed697',
        backgroundColor: '#ffffff',
        disableAutoPan: true,
        hideCloseButton: true,
        maxWidth: 140,
        minHeight: 50,
        maxHeight: 160,
        arrowPosition: 50,
        backgroundClassName: 'phoney',
        arrowStyle: 0
    });

    google.maps.event.addListener(marker, 'click', function () { 
      infoBubble.open(map, marker);
    });
    
    google.maps.event.addListener(map, 'click', function () { 
      infoBubble.close();
    });

    return marker;
  }
}

function get_my_locations(){
   /*var places = $.parseJSON(data);
  console.log(places);*/
  
  
}


function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
}

function area_finder_ajax_load(){
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      $lat = position.coords.latitude;
      $long = position.coords.longitude;  
      
      $("#ajax-target").load("/areafinder/" + position.coords.latitude + "/" + position.coords.longitude);   
    });    
  }
}

function area_finder_user(){ 
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var $basepath = Drupal.settings.basePath,   
          userlat = position.coords.latitude,
          userlng = position.coords.longitude;
            
      /*$.ajax({
        url: $basepath + 'areafinder/json/' + userlat + '/' + userlng,        
        success: function(data) {
          console.log(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
          alert("Status: " + textStatus); alert("Error: " + errorThrown); 
        } 
      });  
      
      $.ajax({
        url: $basepath + 'areafinder/map/' + userlat + '/' + userlng,        
        success: function(data) {
          console.log(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
          alert("Status: " + textStatus); alert("Error: " + errorThrown); 
        } 
      });*/
      
      $.ajax({
        url: $basepath + 'areafinder/geo/' + userlat + '/' + userlng,        
        success: function(data) {
          console.log(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
          alert("Status: " + textStatus); alert("Error: " + errorThrown); 
        } 
      });
      
    });    
  } 
}



function area_finder_places(){
  
  
  /*if( $(".google-map-field").length ){
    var getPlaceLat = $('.google_map_field_display').attr('data-lat');
    var getPlaceLng = $('.google_map_field_display').attr('data-lng');
    
    var placelatlng = new google.maps.LatLng(getPlaceLat, getPlaceLng);
    //console.log(Latlng);
    
    //area_finder_distance('', placelatlng);
    $( "body" ).data("placelat", getPlaceLat).data("placelng", getPlaceLng);
    //return $.data("place_coords", placelatlng);
    //return Placelatlng;
    area_finder_distance();
  }*/
  
  /*var lat = 51.528009;
  var lng = -0.055432;
  if( $('.field-name-field-place-address').length > 0 ){
    var address = ;
    console.log('address: ' + address);
  }
  
  
  var latlng = new google.maps.LatLng(lat, lng);
  var geocoder= new google.maps.Geocoder();
  
  geocoder.geocode({ 'latLng': latlng }, function (results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      var position = results[0];
      //console.log(position);
    }
  });*/
}



function area_finder_distance(){
  
  /*var ulat = $( "body" ).data("userlat");
  var ulng = $( "body" ).data("userlng");
  
  var plat = $( "body" ).data("placelat");
  var plng = $( "body" ).data("placelng");
  
  var earthRadius = 6371; //sets units to km
    
  var lat = plat-ulat; // Difference of latitude
  var lon = plng-ulng; // Difference of longitude

  var disLat = (lat*Math.PI*earthRadius)/180; // Vertical distance
  var disLon = (lon*Math.PI*earthRadius)/180; // Horizontal distance

  var ret = Math.pow(disLat, 2) + Math.pow(disLon, 2); 
  ret = Math.sqrt(ret).toFixed(2); // Total distance (calculated by Pythagorus: a^2 + b^2 = c^2)
  
  //console.log(ret);
  
  if(ret <= 5){
    console.log("place is nearby, dist: " + ret + "km");
  } else {
    console.log("place is far, dist: " + ret + "km");    
  }
  //return ret;
  //TODO send via ajax to php
  */
}

function make_links(target){
  
  var base = 'www.exebenus.com',
      fb = '<a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//www.exebenus.com/"  target="_blank" id="fb_share">Share on facebook</a><br/>',
      tw = '<a href="https://twitter.com/home?status=http%3A//www.exebenus.com/"  target="_blank" id="tw_share">Share on twitter</a><br/>',
      li = '<a href="https://www.linkedin.com/shareArticle?mini=true&url=http%3A//www.exebenus.com/" target="_blank" id="li_share">Share on linkedin</a><br/>',
      mt = '<a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site http://www.exebenus.com" title="Share by Email" id="mt_share">Share by Email</a>',
  
      s_box = '<span class="exe_social_share">' + fb + tw + li + mt + '</span>'
  
  $(target).prepend(s_box);
}
