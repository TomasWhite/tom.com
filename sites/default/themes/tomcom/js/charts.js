/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
$ = jQuery;
(function ($, Drupal, window, document, undefined) {


// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.my_custom_behavior = {
  attach: function(context, settings) {

    // Place your code here.

  }
};


})(jQuery, Drupal, this, this.document);


$( document ).ready(function() {
  
  cdc_stats();
  
  //google_chart_edit();
  
  jq_plot_line();
  jq_plot_pie();
  
  chart_js();      
  
});

function cdc_stats(){
  
  if( $(".node-stats .tablefield").length ){
    
    var contentArray = new Array();

    $('.tablefield tr').each(function(indexParent) {
      contentArray[indexParent] = new Array();
        $(this).children().each(function(indexChild) {
          contentArray[indexParent][indexChild] = $(this).html();
        });
    });
    
    /*
    [contentArray]:
          col0      col1
    row0: children  value
    row1: Benefit   75
    row2: not       25
    */    
    
    var head = contentArray['0'];
    
    if (head.length == 2){
      cdc_pie(contentArray); 
    } else if (head.length > 2){
      cdc_chart(contentArray); 
    } else {
      console.log("check number of columns") 
    }    
  }  
}

function cdc_pie(contentArray){
  
  var head = contentArray['0'];
  
  var i = 0,
      l = contentArray.length;

  var dataPie = new google.visualization.DataTable();
  dataPie.addColumn('string', head['0']);
  dataPie.addColumn('number', head['1']);

  for(i ; i < l ; i++){
    if( !($.isEmptyObject( contentArray[(i+1)] )) ){
      dataPie.addRows([
        [contentArray[(i+1)]['0'], parseInt(contentArray[(i+1)]['1'])], 
      ]);
    }
  }

  /*NB: var dataArray isn't quite working, something to do with numbers as strings maybe?*/
  var dataArray = google.visualization.arrayToDataTable([
    [
      {label: contentArray['0']['0'], id: contentArray['0']['0']},
      {label: contentArray['0']['1'], id: contentArray['0']['1'], type: 'number'}, // Use object notation to explicitly specify the data type.
    ],
    contentArray['1'],
    contentArray['2']
    ],
    false);
  /*end var dataArray*/

  var chart_type = $('.field-name-field-google-chart-type .field-items .even').html();
  
  var options = {
    width: 400,
    pieStartAngle: 90,
    legend: {
      position: 'labeled',
      color: 'red',
      textStyle: {
        color: 'red',
      },
    },
    pieSliceText: 'none',    
  };
  
  if(chart_type == 'donut'){
    var options = {
      pieHole: 0.5,
      width: 400,
      pieStartAngle: 90,
      legend: {
        position: 'labeled',
        color: 'red',
        textStyle: {
          color: 'red',
        },
      },
      pieSliceText: 'none',  
    };
  }
  

      
  var chartPie = new google.visualization.PieChart(document.getElementById('donutchart'));
  chartPie.draw(dataPie, options);
}

function cdc_chart(contentArray){
  
  var head = contentArray['0'];
  
  var i = 0,
      l = contentArray.length;
  // Column Headers
  var dataChart = new google.visualization.DataTable();
  for(i ; i < l ; i++){
    if( !($.isEmptyObject( contentArray[i] )) ){
      dataChart.addColumn('number', head[i]);
    }
  }
  
  // Rows
  for(i ; i < l ; i++){
    if( !($.isEmptyObject( contentArray[(i+1)] )) ){
  console.log([parseInt(contentArray[(i+1)]['0']), parseInt(contentArray[(i+1)]['1']), parseInt(contentArray[(i+1)]['2']), parseInt(contentArray[(i+1)]['3']), parseInt(contentArray[(i+1)]['4'])]);
      dataChart.addRows([
        [parseInt(contentArray[(i+1)]['0']), parseInt(contentArray[(i+1)]['1']), parseInt(contentArray[(i+1)]['2']), parseInt(contentArray[(i+1)]['3']), parseInt(contentArray[(i+1)]['4'])], 
      ]);
    }
  }
  
  var chart = new google.visualization.LineChart(document.getElementById('chart2'));
  chart.draw(dataChart);
}

function google_chart_edit(){
  var wrapper_tjw = new google.visualization.ChartWrapper({
    chartType: 'ColumnChart',
    'dataSourceUrl': 'https://docs.google.com/spreadsheets/d/1UR-cZlicvDRtDBF88y5-bVYCeq_iNvS5rqbC9t1fAMI/edit#gid=0',
    containerId: 'create_g_chart'
  });    
  
  wrapper_tjw.draw();
  
  $("#chart_edit").click(function(){
    edit_wrapper(wrapper_tjw);
  });  
}

function edit_wrapper(wrapper_tjw){  
  
  chartEditor = new google.visualization.ChartEditor();
  google.visualization.events.addListener(chartEditor, 'ok', redrawChart);
  chartEditor.openDialog(wrapper_tjw, {dataSourceInput: 'urlbox'});
}

// On "OK" save the chart to a <div> on the page.
function redrawChart(){
  chartEditor.getChartWrapper().draw(document.getElementById('create_g_chart'));
}


//function called via callback from html.tpl.php when including google charts library
function drawChart(){
  
  var line_query = new google.visualization.Query(
    'https://docs.google.com/spreadsheets/d/1UR-cZlicvDRtDBF88y5-bVYCeq_iNvS5rqbC9t1fAMI/edit#gid=0'
  );  
  line_query.send(line_response); 
  
  var pie_query = new google.visualization.Query(
    'https://docs.google.com/spreadsheets/d/1UR-cZlicvDRtDBF88y5-bVYCeq_iNvS5rqbC9t1fAMI/edit#gid=1312016469'
  );
  pie_query.send(pie_response);
}

function line_response(response) {
  if (response.isError()) {
    alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
    return;
  }

  var data = response.getDataTable();
  var options = { 
    height: 400,
    vAxis: {
      viewWindowMode: 'maximized',
    },
    animation: {
      "startup": true,
      duration: 1000,
      easing: 'out',
    },
  }
  var line_chart = new google.visualization.ColumnChart(document.getElementById('g_chart'));
  line_chart.draw(data, options);
}

function pie_response(response){
  if (response.isError()) {
    alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
    return;
  }

  var data = response.getDataTable();
  var options = { 
    is3D: true,
    title: 'Pie\'s cannot be animated'
  }
  var pie_chart = new google.visualization.PieChart(document.getElementById('pie_g_chart'));  
  pie_chart.draw(data, options);
}


function jq_plot_line(){
  if ($("#tjw_jqplot_line").length){
    $.jqplot('tjw_jqplot_line', 
      [[[1, 2],[3,5.12],[5,13.1],[7,33.6],[9,85.9],[11,219.9]]],
      { 
        animate: true
      }
    ); 
  }
}
function jq_plot_pie(){
  if ($("#tjw_jqplot_pie").length){
    $.jqplot.config.enablePlugins = true;
    $.jqplot('tjw_jqplot_pie', 
      [[['Verwerkende industrie', 9],['Retail', 8], ['Primaire producent', 7], 
      ['Out of home', 6],['Groothandel', 5], ['Grondstof', 4], ['Consument', 3], ['Bewerkende industrie', 2]]], 
      {
        title: ' ', 
        animate: true,
        seriesDefaults: {shadow: true, renderer: $.jqplot.PieRenderer, rendererOptions: {
          showDataLabels: true, 
          fill: false,
          sliceMargin: 4
          } 
        }, 
        legend: { show:true }
      }
    );
  }
}

function chart_js(){
  
  Chart.defaults.global.responsive = true;
  
  var line_data = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
      {
        label: "My First dataset",
        fillColor: "rgba(220,220,220,0.2)",
        strokeColor: "rgba(220,220,220,1)",
        pointColor: "rgba(220,220,220,1)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(220,220,220,1)",
        data: [65, 59, 80, 81, 56, 55, 40]
      },
      {
        label: "My Second dataset",
        fillColor: "rgba(151,187,205,0.2)",
        strokeColor: "rgba(151,187,205,1)",
        pointColor: "rgba(151,187,205,1)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(151,187,205,1)",
        data: [28, 48, 40, 19, 86, 27, 90]
      }
    ]
  };  
  
  var pie_data = [
    {
      value: 300,
      color:"#F7464A",
      highlight: "#FF5A5E",
      label: "Red"
    },
    {
      value: 50,
      color: "#46BFBD",
      highlight: "#5AD3D1",
      label: "Green"
    },
    {
      value: 100,
      color: "#FDB45C",
      highlight: "#FFC870",
      label: "Yellow"
    }
  ]
  
  if($("#chart_js_line").length){
    // Get context with jQuery - using jQuery's .get() method.
    var line_ctx = $("#chart_js_line").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    // new Chart(ctx).Line(data, options);
    var line_chart = new Chart(line_ctx).Line(line_data, {
        scaleOverride : true,
        scaleSteps : 20,
        scaleStepWidth : 5,
        scaleStartValue : 0 
    });  
    var line_legend = line_chart.generateLegend();
    $("#line_legend_place").append(line_legend);
  }
  if($("#chart_js_pie").length){
    var pie_ctx = $("#chart_js_pie").get(0).getContext("2d");  
    var pie_chart = new Chart(pie_ctx).Pie(pie_data);  
    var pie_legend = pie_chart.generateLegend();
    $("#pie_legend_place").append(pie_legend);
  }
}
