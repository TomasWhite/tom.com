This module allows you to change the style of the ip_geoloc default infowindow.

In order to get this module to work, need ip_geoloc enabled https://www.drupal.org/project/ip_geoloc

The ip_geoloc has been modified to get the infowindow variable available for use in this module.
In modules > ip_geoloc > js > ip_geoloc_gmap_multi_loc.js line 157, the addMarkerBalloon function has been added to. 

===
OLD
===

function addMarkerBalloon(map, marker, infoText) {
  google.maps.event.addListener(marker, 'click', function(event) {
    if (infoWindow) {
      infoWindow.close();
    }
    infoWindow.setOptions({
      content: infoText,
      // See [#1777664].
      maxWidth: 200
    });
    infoWindow.open(map, marker);
  });
}

===
NEW
===

function addMarkerBalloon(map, marker, infoText) {
  google.maps.event.addListener(marker, 'click', function(event) {
    if (infoWindow) {
      infoWindow.close();
    }
    infoWindow.setOptions({
      content: infoText,
      // See [#1777664].
      maxWidth: 200
    });
    infoWindow.open(map, marker);
    //call to ip_geoloc_infowindow_fix
     if (typeof mypopupfix !== 'undefined' && $.isFunction(mypopupfix)) {
      mypopupfix(infoWindow);  
    } else {
      console.log('ip_geoloc_infowindow_fix module not enabled'); 
    }         
    //END call  
  });
}
