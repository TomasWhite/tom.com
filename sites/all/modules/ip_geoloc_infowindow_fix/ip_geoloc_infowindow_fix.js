function mypopupfix(infoWindow){
  //checks if ip_geoloc has loaded map on page
  if($('.ip-geoloc-map').length){
    google.maps.event.addListener(infoWindow, 'domready', function() {
      /*domready from: http://en.marnoto.com/2014/09/5-formas-de-personalizar-infowindow.html*/
      
      // Reference to the DIV which receives the contents of the infowindow using jQuery
      var iwOuter = $('.gm-style-iw');

      /* The DIV we want to change is above the .gm-style-iw DIV.
      * So, we use jQuery and create a iwBackground variable,
      * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
      */
      var iwBackground = iwOuter.prev();

      // Add class to infowindow background div for ease of debugging  
      iwBackground.addClass('infowindow_background'); 

      // Add class to infowindow div to change background styles  
      iwBackground.children().addClass('infowindow_fix'); 

      // Add class to tail to change size of tail
      iwBackground.children(':nth-child(3)').children().addClass('infowindow_fix_tail');

      // Add class to infowindow tail to change border   
      iwBackground.children(':nth-child(3)').find('div').children().addClass('infowindow_fix_tail_border');

      // Add class to close button on infowindow
      iwOuter.next().addClass('infowindow_close');  
    });
  } else {
    console.log('ip_geoloc module missing/altered, see README.txt'); 
  }
}