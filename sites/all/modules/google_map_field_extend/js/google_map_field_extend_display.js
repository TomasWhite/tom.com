
/**
 * @file
 * This file contains the javascript functions used to display a map when the
 * entity it is attached to is displayed.
 */

/**
 * Declare global variable by which to identify the map.
 */
var google_map_field_maps = new Array();

var google_map_pins = {"pin":"/sites/default/themes/teamuporguk/images/map_pin/map_pin.png", 
"pin_active":"/sites/default/themes/teamuporguk/images/map_pin/map_pin_active.png", 
"pin_current":false };


/**
 * Add code to generate the map on page load.
 */
(function ($) {
  Drupal.behaviors.google_map_field_extend = {
    attach: function (context) {
      // Pick up all elements of class google_map_field and loop
      // through them calling the google_map_field_load_map function
      // with the object ID.
      $(".google_map_field_display_extend").each(function(index, item) {
        var objId = $(item).attr('id');
				$(item).css('min-width', 200);
				$(item).css('min-height', 200);
        google_map_field_extend_load_map(objId);
      });
    }
  };
})(jQuery);

/**
 * This function is called by the google_map_field Drupal.behaviour and
 * loads a google map in tot he given map ID container.
 */
function google_map_field_extend_load_map(map_id) {
	//alert(map_id);
	//console.log(Drupal.settings.gmfe_node_display);
  // Get the settings for the map from the Drupal.settings object.
  var lat = Drupal.settings.gmfe_node_display[map_id]['mid']['lat'];
  var lon = Drupal.settings.gmfe_node_display[map_id]['mid']['lon'];
  var zoom = parseInt(Drupal.settings.gmfe_node_display[map_id]['mid']['zoom']);
	
  // Create the map coords and map options.
  var latlng = new google.maps.LatLng(lat, lon);

	var json_style;
	json_style = Drupal.settings.gmfe_node_display[map_id]['json'];
  
  if(!json_style){
    zoom = 14;
    json_style = [
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "color": "#3f484a"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "administrative.neighborhood",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "administrative.neighborhood",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "color": "#3f484a"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ebf0ef"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ebf0ef"
            }
        ]
    },
    {
        "featureType": "landscape.natural.landcover",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "color": "#ebf0ef"
            }
        ]
    },
    {
        "featureType": "landscape.natural.terrain",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "color": "#ebf0ef"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.attraction",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.school",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "color": "#3f484a"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#b2c7d1"
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "saturation": "100"
            },
            {
                "lightness": "100"
            },
            {
                "gamma": "10.00"
            },
            {
                "weight": "10.00"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "saturation": "100"
            },
            {
                "lightness": "100"
            },
            {
                "gamma": "10.00"
            },
            {
                "weight": "10.00"
            }
        ]
    }
];

  }
  
  
	if(json_style){
		// Create a new StyledMapType object, passing it the array of styles,
	  // as well as the name to be displayed on the map type control.
    var json_style_object = {};
    if(typeof json_style === 'object'){
      json_style_object = json_style;
    } else {
  		//eval('var json_style_object = '+json_style);
    	eval('var json_style_object = '+json_style);
    }
	  // Create a new StyledMapType object, passing it the array of styles,
  	// as well as the name to be displayed on the map type control.
  	var styledMap = new google.maps.StyledMapType(json_style_object,
    	{name: "Styled Map"});

  	// Create a map options, and include the MapTypeId to add
  	// to the map type control.
		/*
			TODO add settigns true/false
			mapTypeControl 
			panControl 
			zoomControl
			scaleControl 
		*/
		
  	var mapOptions = {
    	scrollwheel: false,
      zoom: zoom,
    	center: latlng,
			streetViewControl: false,
			mapTypeControl: false,
			panControl: false,
			zoomControl: false,
    	mapTypeControlOptions: {
      	mapTypeIds: []
    	}
  	};	
		
	} else {
		//Create a normal map options
		var mapOptions = {
    	scrollwheel: false,
			zoom: zoom,
			center: latlng,
			streetViewControl: false,
			panControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		
	}

  // Create the map.
  google_map_field_maps[map_id] = new google.maps.Map(document.getElementById(map_id), mapOptions);

	
	if(json_style){
		//Apply the styled map  
		google_map_field_maps[map_id].mapTypes.set('map_style', styledMap);
		google_map_field_maps[map_id].setMapTypeId('map_style');
	}
	
	/*
	var myOptions = {
    content: 'loading...'
  }
	
	var extend_infowindow = new google.maps.InfoWindow(myOptions);
	*/
	
// map: map,
// backgroundColor: 'rgb(57,57,57)',
// position: new google.maps.LatLng(-35, 151),
// hideCloseButton: true,

	//http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobubble/examples/example.html?compiled
	/*infoBubble2 = new InfoBubble({
          content: '<div class="phoneytext">Some label</div>',
          shadowStyle: 0,
          padding: 10,
          borderRadius: 4,
          arrowSize: 0,
          borderWidth: 1,
          borderColor: '#cccccc',
          disableAutoPan: true,
					hideCloseButton: true,
					maxWidth: 100,
					minHeight: 20,
          arrowPosition: 0,
          backgroundClassName: 'phoney',
          arrowStyle: 0
        });*/

	infoBubble2 = new InfoBubble({
          content: '<div class="phoneytext">Some label</div>',
          shadowStyle: 0,
          padding: 12,
          borderRadius: 4,
          arrowSize: 7,
          borderWidth: 1,
          borderColor: '#d7d7d7', //'#0ed697',
					backgroundColor: '#ffffff',
          disableAutoPan: true,
					hideCloseButton: true,
					maxWidth: 140,
					minHeight: 50,
          arrowPosition: 50,
          backgroundClassName: 'phoney',
          arrowStyle: 0
        });
//fff4d9

	//if visible clear the list box
	if( jQuery('#block-google-map-field-extend-gmfe-map-block-info-list .content').length ){
		jQuery('#block-google-map-field-extend-gmfe-map-block-info-list .content').html("");
	}
	
	// find all the markers for a map
	jQuery(Drupal.settings.gmfe_node_display[map_id]['points'] ).each(function(idx, ido){
		add_marker(google_map_field_maps[map_id], ido, idx, infoBubble2, map_id);		
	});

	// Click to centre 
	set_all_maker_click_event();
	
	// form that centers map
	set_map_center_form();
  
  if( jQuery('#block-google-map-field-extend-gmfe-map-block-info-block').length ){
     jQuery('#block-google-map-field-extend-gmfe-map-block-info-block').prepend('<a class="close_button">X<a>');
     
     var menu_copy = jQuery('.menu-mlid-440 > ul').html();
     menu_copy = "<div id='gmfe_menu_patch' class='fake_menu_wrap'><h4>Browse schools by area</h4><div id='menu_left'><ul>"+menu_copy+"</ul></div><div id='menu_right'><ul></ul></div></div>";
     
     jQuery('#map_pre').prepend(menu_copy);
     jQuery('#gmfe_menu_patch a').addClass('fake_menu_item');
     
     //move 'second half' of menu .menu-mlid-485 School with spaces
     jQuery('#menu_right > ul').append( jQuery('#menu_left .menu-mlid-485').prev('li').nextAll('li') );
     
     
     
     jQuery('#content .node_inner_wrapper .node_content .node_content_inner').append( jQuery('#map_pre') );
     
     jQuery('#block-google-map-field-extend-gmfe-map-block-info-block .close_button').click(function(){
      jQuery('#block-google-map-field-extend-gmfe-map-block-info-block').removeClass('open'); 
      jQuery('#block-google-map-field-extend-gmfe-map-block-info-block .content').html("Click on a pin to load information, it will appear here.");
      
      if( google_map_pins.pin_current ){
        google_map_pins.pin_current.setIcon( google_map_pins.pin );
      }
      google_map_pins.pin_current = false;
      
     })
  }
}

function set_map_center_form(){
	if( jQuery('#centre_map_form_text').length ){
		var form_text = "Location search";
		//alert(form_text);
		jQuery('#centre_map_form_text').val(form_text);

		jQuery('#centre_map_form_text').focus(function(){
			if($(this).val() == form_text){
				$(this).val("");
			}
			jQuery('#centre_map_form_text').addClass('active');
		}).blur(function(){
			if(jQuery(this).val() == ""){
				jQuery(this).val(form_text);
				jQuery('#centre_map_form_text').removeClass('active');
			}
		});
	}
}

//If the list all block exists then all the click event for 'click to center'
function set_all_maker_click_event(){
	//if visible add click events for the list of all markert
	if( jQuery('#block-google-map-field-extend-gmfe-map-block-info-list .content  a.centre_map').length ){
		jQuery('#block-google-map-field-extend-gmfe-map-block-info-list .content a.centre_map').click(function(){
			var idx = $(this).attr('idx');
			google.maps.event.trigger(marker_list[idx].marker, 'click');
			return false;
		});
	} else {
		//note if no points exist remove the block
		jQuery('#block-google-map-field-extend-gmfe-map-block-info-list').remove;
	}
}

/*
add a point to 'map'
ido has 
  lat, lon, title, nid
idx 
  is an individual pin index for this map
*/
// Array of all markers
var marker_list = new Array();
// Array of all nodes note just an index for the above array
var marker_list_nid = new Array();
function add_marker(map, ido, idx, infowindow, map_id){
	
		//console.log(ido);
		//console.log(idx);
	
		var marker = make_marker(ido, map, map_id);
		
		//Save some extra info for the popup
		marker.idx = idx;
		marker.nid = ido['nid'];
		marker.title = ido['title'];
		marker.type = ido['type'];
		marker.html_content = ido['html_content'];
		
		add_marker_onclick(map, marker, infowindow);
		
		//record the marker and load the content
		marker_list[idx] = {marker: marker, content: ''};
		var content = get_maker_content(marker);
		//marker_list[idx]['content'] = content;
		marker_list_nid[ido['nid']] = idx;
	
		//TODO look for all point list block and/or visible point block
		if( jQuery('#block-google-map-field-extend-gmfe-map-block-info-list .content').length ){
			content = content + ' - <a class="centre_map" href="#" idx="'+idx+'">Click to centre</a><br/>';
			jQuery('#block-google-map-field-extend-gmfe-map-block-info-list .content').append(content);
		}
}

// create a map object and add that to a map
function make_marker(ido, map, map_id){
	//console.log(map);
	var latlng = new google.maps.LatLng(ido['lat'], ido['lon']);
	
	// Define Marker properties
	//TODO select this based on a setting
	var img_path = '/sites/all/modules/google_map_field_extend/map/pin_blue.png?orig';
	var pin_object, size = '', orign = '', point = '';
	var pin_shodow;
	
	//console.log(map_id);
	//console.log(Drupal.settings.gmfe_node_display);
	//console.log(Drupal.settings.gmfe_node_display[map_id]);

	// Find default pin settings
	if( Drupal.settings.gmfe_node_display[map_id].pins && Drupal.settings.gmfe_node_display[map_id].pins.default_pin ){
		pin_object = Drupal.settings.gmfe_node_display[map_id].pins.default_pin;

		img_path = pin_object.img;
		size = new google.maps.Size(pin_object.width, pin_object.height);
		orign = new google.maps.Point(0,0);
		point = new google.maps.Point(pin_object.point_x, pin_object.point_y);

		pin_shodow = Drupal.settings.gmfe_node_display[map_id].pins.default_pin;

	} else if( Drupal.settings.gmfe_node_display.pins.default_pin ){
		pin_object = Drupal.settings.gmfe_node_display.pins.default_pin;
		
		img_path = pin_object.img;
		size = new google.maps.Size(pin_object.width, pin_object.height);
		orign = new google.maps.Point(0,0);
		point = new google.maps.Point(pin_object.point_x, pin_object.point_y);

		pin_shodow = Drupal.settings.gmfe_node_display.pins.default_pin;
	}

	// Find node specific pin settings
	if( Drupal.settings.gmfe_node_display[map_id].pins && Drupal.settings.gmfe_node_display[map_id].pins[ido['type']] ){
		pin_object = Drupal.settings.gmfe_node_display[map_id].pins[ido['type']];

		img_path = pin_object.img;
		size = new google.maps.Size(pin_object.width, pin_object.height);
		orign = new google.maps.Point(0,0);
		point = new google.maps.Point(pin_object.point_x, pin_object.point_y);
		
	} else if( Drupal.settings.gmfe_node_display.pins[ido['type']] ){
		pin_object = Drupal.settings.gmfe_node_display.pins[ido['type']];

		img_path = pin_object.img;
		size = new google.maps.Size(pin_object.width, pin_object.height);
		orign = new google.maps.Point(0,0);
		point = new google.maps.Point(pin_object.point_x, pin_object.point_y);
	} 
	
	//TODO user / roles / taxonomy ?
	
	//console.log(ido);
	//TODO optional shodow
		var shadow = "";
    if(img_path){
      shadow = new google.maps.MarkerImage("/sites/default/themes/teamuporguk/images/map_pin/map_pin_shadow.png",
            size,
            orign,
            point
      );
    }
		 /*
		 img_path = "";
   */
   
	  var image = "";
	 	if(img_path){
			image = new google.maps.MarkerImage(img_path,
					size,
					orign,
					point
			);
		}
		
		
		/*var image = new google.maps.MarkerImage(img_path,
        new google.maps.Size(21, 31),
        new google.maps.Point(0,0),
        new google.maps.Point(10, 31)
    );*/

	 // Add Marker
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon: image,
				shadow: shadow,
				title: ido['nid']+': '+ido['title']
    });
	
	return marker;
}

//add an event listner to a marker
// center the map
// load content
function add_marker_onclick(map, marker, infowindow){
  
	google.maps.event.addListener(marker, 'click', function() {	
		map.setCenter(marker.getPosition());
    marker_info(map, marker, infowindow);
    
    if( google_map_pins.pin_current ){
        google_map_pins.pin_current.setIcon( google_map_pins.pin );
    }
    google_map_pins.pin_current = marker;
	  google_map_pins.pin_current.setIcon( google_map_pins.pin_active );
    	
	});
	
}

//Get the content for the maker 
//Use to fill the pop-up window, the info block, the all items list and all visible list
function get_maker_content(marker){
	var content = '';
	var found = false;
	
	if(found === false){
		if(marker_list[marker.idx].content){
			content = marker_list[marker.idx].content;
			found = true;
			if(content == ''){	
				found = false;
			} 
		} else {
			found = false;
		}
	}

  //TODO ONLY USE WHEN alt popup is avaliabel
  if( jQuery('#block-google-map-field-extend-gmfe-map-block-info-block').length ){
  
    if(found === false){
      if(marker.html_content){
        if(marker.html_content != ""){
          found = true;
          content = marker.html_content;
          marker_list[marker.idx].content = content;
          return content;
        }
      }
      //marker.html_content = ido['html_content'];
    }
    
  }
  

	if(found === false){
		var my_listing_box = jQuery('.region-content .block .list_nid_'+marker.nid);
		if( my_listing_box.length ){
			//alert('hs'+marker.nid);
			content = content+'<a class="title_link" href="/node/'+marker.nid+'">';
			content = content+marker.title;
			content = content+'</a>';
			
			if( my_listing_box.find('.address_box').length ){
				//alert('hs 1');
				content = content + my_listing_box.find('.address_box').html();
				//found = true;
			} 
			if( my_listing_box.find('.website').length ){
				//alert('hs 2');
				var address = my_listing_box.find('.website').text();
				content = content + "<span class='address'>Website: <a href='"+address+"' target='_blank'>"+address+"</a></span>";
				//found = true;
			} 
			var type = my_listing_box.find('.type').text();
			content = "<div class='map_info map_"+type+"'>"+content+"</div>";
			found = true;
		} else {
			//alert('.region-content .block .list_nid_'+marker.nid);
		}
	}
  

	if(found === false){
		//TODO look for ajax content loading
		content = '';
		content = content+'<a class="unfound type_'+marker.type+'" href="/node/'+marker.nid+'">';
		content = content+marker.title;
		content = content+'</a>';
		marker_list[marker.idx].content = content;
	}
	
	return content;
}




//Find the content for a node and populate popup / other
function marker_info(map, marker, infowindow){
	var content = get_maker_content(marker);

	//Look for alternative to infowindow
	var show_map_popup = true;
	if( jQuery('#block-google-map-field-extend-gmfe-map-block-info-block').length ){
		//TODO make this a setting, may possible want to popup infowindow as well???
		show_map_popup = false;
		jQuery('#block-google-map-field-extend-gmfe-map-block-info-block .content').html(content);
        jQuery('#block-google-map-field-extend-gmfe-map-block-info-block').addClass('open');
        //TODO re_check_svg()
        if (typeof re_check_svg == 'function') { 
          re_check_svg();
        } else {
          jQuery('#block-google-map-field-extend-gmfe-map-block-info-block').addClass('no_re_check_svgcd');
        }
    
    var scroll_to = jQuery("#map_target").offset().top;
    
    if( jQuery('.width-holder-header').length ){
      scroll_to = scroll_to - jQuery('.width-holder-header').height();
    }
    if( jQuery('#admin-menu').length ){
      scroll_to = scroll_to - jQuery('#admin-menu').height();
    }
    
    if(jQuery(document).scrollTop() <  scroll_to) {
      
      jQuery('html, body').animate({
        scrollTop: scroll_to
      }, 750);
      
    }
    
	} 
	
	if(show_map_popup){
		infowindow.setContent('Loading ...');
		infowindow.setContent( content );
		infowindow.open(map,marker);
		
		//TODO Add classes //marker_list[marker.idx].content;???
		$('.phoney').parent('div').css('height', 'auto' );
		$('.phoney').addClass('type');
	} else {
		infowindow.close();
	}
}


/**
 * This function takes the value in the center on field and centers the map
 * at that point. BUT does not add a new marker
 */

function google_map_field_extent_doCenter() {
	var geocoder = new google.maps.Geocoder();
  /*
	var fname = Drupal.settings.gmf_widget_form['fname'];
  var location = document.getElementById("edit-"+fname+"-und-0-center-on").value;
	*/
	var location = 'e9';
	if( $('#centre_map_form_text').length ){
		location = $('#centre_map_form_text').val();
	}
	location = location+", UK";
	
  geocoder.geocode( { 'address': location}, function (result, status) {
    if (status == 'OK') {
      var latlng = new google.maps.LatLng(result[0].geometry.location.lat(), result[0].geometry.location.lng());
			
			//console.log(result[0]);
			
			//TODO think about center all maps on the page???
			$('.google_map_field_display_extend').each(function(){
				var map_id = $(this).attr("id");
				//center map
				google_map_field_maps[map_id].panTo(latlng);

				// zoom to area
				//Un-comment test google_map_field_maps[map_id].fitBounds(result[0].geometry.viewport);
				
			});
    } else {
      alert('Could not find location.');
    }
    return false;
  });
  return false;
}