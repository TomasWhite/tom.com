
var arrMarkers = []; 
 
(function ($) {
  Drupal.behaviors.gmap_multipin = {
    attach: function (context, settings) { 
    
    // change this value for the id of the div containing the map  
    var map_id = "gmap_multipin"; 
      
    //<div id='gmap_multipin' style='width: 500px; height: 400px;'></div>  
    var mapCanvas = document.getElementById(map_id);
      
    //only run script if map container div is present on page, (prevents Cannot read property 'offsetWidth' of null error)
    if(mapCanvas){
      var placeData = Drupal.settings.gmap_multipin['placeData'];    
      
      //var placeData = Drupal.settings.gmap_multipin['placeData'];    
      var infoBubble2 = new InfoBubble({
              //http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobubble/examples/example.html/
                //content:infocontent, //moved to 'click' event listener 
                shadowStyle: 0,
                padding: 12,
                borderRadius: 0,
                arrowSize: 7,
                borderWidth: 1,
                borderColor: '#d7d7d7', //'#0ed697',
                backgroundColor: '#ffffff',
                disableAutoPan: true,
                hideCloseButton: true,
                maxWidth: 140,
                minHeight: 50,
                maxHeight: 160,
                arrowPosition: 50,
                backgroundClassName: 'phoney',
                arrowStyle: 0
            });   

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          userlat = position.coords.latitude,
          userlng = position.coords.longitude;

          var lat = userlat;
          var lon = userlng;
          var zoom = 14;

          // Create the map coords and map options.
          var latlng = new google.maps.LatLng(lat, lon);
          var mapOptions = {
            scrollwheel: false,
            zoom: zoom,
            center: latlng,
            streetViewControl: false,
            mapTypeControl: false,
            panControl: false,
            zoomControl: true,
            scaleControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles:[
            {
                "featureType": "administrative",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "simplified"
                    },
                    {
                        "color": "#3f484a"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.province",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative.neighborhood",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative.neighborhood",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "simplified"
                    },
                    {
                        "color": "#3f484a"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    },
                    {
                        "weight": "1.00"
                    },
                    {
                        "color": "#ff0000"
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#d9e5ed"
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "visibility": "off"
                    },
                    {
                        "color": "#cbdce7"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "hue": "#0053ff"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "simplified"
                    },
                    {
                        "saturation": "100"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#afcadb"
                    }
                ]
            },    
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#d3e1ea"
                    },
                    {
                        "gamma": "0.48"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "hue": "#0053ff"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            }
        ]
          };  
          var map = new google.maps.Map(mapCanvas, mapOptions);
          var $basepath = Drupal.settings.basePath; 

          // Create the user marker
          var user_marker = new google.maps.Marker({
              map: map,
              position: new google.maps.LatLng(userlat, userlng),
              icon: '/sites/default/themes/tomcom/images/map_pin/bf_user_pin.png'
          }); 

          //gets placeData from gmap_multipin.module
          var i;        
          for (i = 0; i < placeData.length; i++) {
            var markerNid = placeData[i]['nid'];

            if(arrMarkers.hasOwnProperty(markerNid)){  
              //DO NOTHING   
            } else {
              var pos = new google.maps.LatLng(placeData[i]['lat'], placeData[i]['lon']);
              var infocontent = placeData[i]['html'];
              var new_marker = addMarker(pos, infocontent); 
              arrMarkers[markerNid] = new_marker;
            } 
          }

          // Create the places marker
          function addMarker (pos, infocontent) {
            var marker = new google.maps.Marker({
                map: map,
                position: pos,
                //zIndex: google.maps.Marker.MAX_ZINDEX + 1, //to put places in front of user pin as was preventing the place to te clicked when user was close to location
                icon: '/sites/default/themes/tomcom/images/map_pin/bf_place_pin.png'
            });    

            google.maps.event.addListener(marker, 'click', function () { 
              infoBubble2.setContent(infocontent);
              infoBubble2.open(map, marker); 
            });

            google.maps.event.addListener(map, 'click', function () { 
              infoBubble2.close();
            });

            return marker;
          }


          //map listeners to determine new map center on drag event
          google.maps.event.addListener(map,'idle',function(){
            if(!this.get('dragging')/* && this.get('latlng') && this.get('latlng')!==this.getCenter()*/) {
              //gets the center of map after drag event and the map bounds
              var dragCenterLat = this.getCenter().lat();
              var dragCenterLon = this.getCenter().lng();

              var dragBounds = this.getBounds();
              var ne = dragBounds.getNorthEast();
              var neBoundLat = ne.lat();
              var neBoundLon = ne.lng();        

              $.ajax({
                url: $basepath + 'multipin-map/drag/' + dragCenterLat + '/' + dragCenterLon + '/' + neBoundLat + '/' + neBoundLon,        
                success: function(data) {
                  var jsonArray = $.parseJSON(data);

                  var i;
                  for (i = 0; i < jsonArray.length; i++) {
                    var markerNid = jsonArray[i]['nid'];

                    if(arrMarkers.hasOwnProperty(markerNid)){  
                      //DO NOTHING
                    } else {
                      var pos = new google.maps.LatLng(jsonArray[i]['lat'], jsonArray[i]['lon']);
                      var infocontent = jsonArray[i]['html'];
                      var new_marker = addMarker(pos, infocontent); 
                      arrMarkers[markerNid] = new_marker;
                    }
                  }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                  alert("Status: " + textStatus); alert("Error: " + errorThrown); 
                } 
              });
            } // END !this.get('dragging') &&...

            if(!this.get('dragging')){
              this.set('latlng',this.getCenter())
            }

            }); // END 'idle' google.maps.event.addListener

            google.maps.event.addListener(map,'dragstart',function(){
              this.set('dragging',true);          
            });        

            google.maps.event.addListener(map,'dragend',function(){
              this.set('dragging',false);
              google.maps.event.trigger(this,'idle',{});
            });

            google.maps.event.addListener(map,'zoom_changed',function(){
              this.set('dragging',false);  
            });
            // END map listeners to determine new map center on drag event

           //Willie fault
          /*google.maps.event.addListener(map, 'click', function () { 
              infoBubble2.close();
          });*/ //outside of marker function clause click event not to be fired

          }); //END getcurrentposition      
        } else {
          alert("Geolocation is not supported by this browser");
          //TODO create better fallback for geolocation fail.
        }
      }
    } //END attach: function
  };
})(jQuery);