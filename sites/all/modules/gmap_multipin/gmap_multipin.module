<?php 

/**
* Implements hook_block_info().
*
* Makes the block to be created for showing the map available via the Structure -> Blocks menu
*/
function gmap_multipin_block_info(){
  $blocks = array();
  
  $blocks['gmap_multipin_block'] = array(
    'info' => 'Gmap Multipin Block',
    'status' => 1,
    'visibility' => BLOCK_VISIBILITY_LISTED,
    'region' => 'sidebar_second',
  );
  
  return $blocks;
}

/**
* Implements hook_block_view().
*
* Creates a block to that will house a map of places based on current user position.
*/
function gmap_multipin_block_view($delta = ''){
  $block = array();
    
  switch ($delta){
    case 'gmap_multipin_block':        
      $block['subject'] = 'Multipin places';        
      $block['content'] = multipin_view();
    break;  
  }
  
  return $block;
}

/**
 * Implements hook_menu().
 * creates a fake menu item to recieve the ajax request from the drag/zoom event on the map
 */
function gmap_multipin_menu() {
  $items = array();  
  
  $items['multipin-map/drag/%/%/%/%'] = array(
    'page callback' => 'multipin_map_drag',
    'access arguments' => array('access content'),
    'page arguments' => array(2,3,4,5), 
    'type' => MENU_CALLBACK,
    'delivery callback' => 'gmap_multipin_callback',
  );
  
  return $items;
}

//TODO check settings for hook_callback, in particular drupal_add_js possibile to add to footer??
//TODO more info needed on hook_callback, not being used properly here
function gmap_multipin_callback($page_callback_result){
  $content = is_string($page_callback_result) ? $page_callback_result :     drupal_render($page_callback_result);  
  
  /*
  if ($content){
    //$content = "<h1>WHU A</h1>".$content;
  } else {
    //$content = "<h1>WHU B</h1>";
    if($page_callback_result){
      $content = "<h1>WHU B</h1>";
      $content .= print_r($page_callback_result, true);
    }
  }
  */
  
  print $content;
  drupal_page_footer();
}

//function to calculate the distance from the map center to the map bounds
function multipin_map_drag($arg2, $arg3, $arg4, $arg5){
  
  //map after dragged center
  $dragLat = $arg2;
  $dragLon = $arg3;
  
  //map after dragged NE bounds
  $dragBoundLat = $arg4;
  $dragBoundLon = $arg5;
  
  //map after dragged radius from center to NE corner of the viewpoint
  $dragDistLat = ($dragBoundLat-$dragLat);
  $dragDistLon = ($dragBoundLon-$dragLon);
  
  $pythagDLat = (pow($dragDistLat, 2));
  $pythagDLon = (pow($dragDistLon, 2));
  
  $dragRadiusSq = ($pythagDLat + $pythagDLon);
  
  _multipin_load_points($dragRadiusSq, $dragLat, $dragLon);  
}

// function containing hard coded values that represent the max distance to show which places are close to the user via geolocation
function multipin_view(){
  
  $userLat = 0.0452185866478557;//Latitude: 1 deg = 110.574 km, 5km = 0.0452185866478557
  $userLon = 0.0721518115495649;//Longitude: 1 deg = 111.320*cos(latitude) km, 5km = 0.0721518115495649

  /*
  1 deg = 111.320km, 
  1/(111.320/5),
  5km = 0.0449155587495508
  $userDist = 0.0449155587495508
  $userDistSq = 0.0449155587495508 ^ 2 = 0.0020174074177843 ~= 5km
  take larger value of lat and lon to use in circle calc to find the places within distance.
  if both latlon values used, creates an ellipse which would potentially exclude relevant places
  */

  $userDist = ( 2*( pow($userLat, 2) ) );
  
  _multipin_load_points($userDist);  
}

/*
* gathers all places created in the view. Then calculates which places to show based on the two criteria. 
* will first load points based on if they are within 5km of the users current location based on html5 geolocation
* secondly will load the places that are in the maps viewpoint when the user either scrolls or zooms out
*/

function _multipin_load_points($userDistSq="0.0020174074177843", $dragLat="", $dragLon=""){ 
  
  $placeData = array();  
  $results = views_get_view_result('bf_multipin', 'block');    
  
  //calculating the center point to base the map calculations on
  if($dragLat == ""){
    //assumes first load as dragLat will be empty until user fires ajax event on drag/zoom
    $centerLat = $results[0]->field_field_geolocation_html5[0]['raw']['lat'];
    $centerLon = $results[0]->field_field_geolocation_html5[0]['raw']['lng'];
  } else {
    $centerLat = $dragLat;
    $centerLon = $dragLon;
  }
  
  if($userDistSq==""){
    //assumes first load and so uses 5km distance limiter
    $maxRadius = "0.0020174074177843";
  } else {
    //drag event triggered, load all places in the viewpoint
    $maxRadius = $userDistSq;
  }  
  
  foreach ($results as $key => $result){

    $placeTitle = $result->node_title;
    $placeNid = $result->nid;
    $placeLat = $result->field_field_geofield[0]['raw']['lat'];
    $placeLon = $result->field_field_geofield[0]['raw']['lon'];
    $placehtml = '<div class="phoneytext"><h2>' . $placeTitle . '</h2><p>nid is ' . $placeNid . ' </p><a href="http://tom.com.testing.effusion.dh.bytemark.co.uk/node/' . $placeNid . '" target="_blank">to the place</a></div>';    
    
    $distanceLat = ($centerLat-$placeLat);
    $distanceLon = ($centerLon-$placeLon);

    $distanceLatSq = pow($distanceLat, 2);
    $distanceLonSq = pow($distanceLon, 2);
    $pythagLatLonSq = ($distanceLatSq+$distanceLonSq); // c^2 = a^2 + b^2

    if($pythagLatLonSq < $maxRadius){
      $placeData[] = array('nid' => $placeNid, 'lat' => $placeLat, 'lon' => $placeLon, 'html' => $placehtml);  
    } else { //TODO: not sure if here or elsewhere, but if pins move outside of viewpoint of user's area, remove the pins from the map until the criteria are met again. $placeData[] = array() has no effect
      $placeData[] = array(); 
    }    
  }
  
  if($dragLat == ""){
    drupal_add_js(array('gmap_multipin' => array('placeData' => $placeData)), 'setting');
    //http://drupal.stackexchange.com/questions/4482/how-to-send-variable-from-php-file-to-js-file-in-drupal-7?lq=1
  
  } else {
    $placeData = drupal_json_encode($placeData);
    print_r($placeData);
  }
}


/**
 * Implements hook_init().
 */
function gmap_multipin_init() {
  drupal_add_js(drupal_get_path('module', 'gmap_multipin') . '/gmap_multipin.js');  
}
