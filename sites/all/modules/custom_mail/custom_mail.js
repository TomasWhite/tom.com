(function($) {
  Drupal.behaviors.custom_mail = {
    attach: function(context, settings){   
      
      $('form', context).once('custom_mail').submit(function(event){    

        var formMessage = $('#edit-message').val(),  
            squareCheck = formMessage.match(/(\[.*?\])/g),
            dollarCheck = formMessage.match(/(\$.?)\w+/g);
        
        if(squareCheck !== null){ 
          $('#custom_mail_error_content').html("check message for squares. Error found: ");
          
          for (var i = 0; i < squareCheck.length; i++) {            
            $("#custom_mail_error_content").append('<li>' + squareCheck[i] + '</li>');
          }          
          $('#custom_mail_error_content li').wrapAll("<ul></ul>");   
          
          $('#edit-submit').attr("value", "fix errors").attr("disabled", true);
          $('#edit-message').addClass("error");
          
          $('#edit-message').focus(function(){
            $('#edit-submit').attr("value", "Send message").attr("disabled", false);
            $('#edit-message').removeClass("error");
          });
                    
          event.preventDefault();
          
        } else if (dollarCheck !== null){
          $('#custom_mail_error_content').text("check message for dollars. Error found: ");
          
          for (var i = 0; i < dollarCheck.length; i++) {            
            $("#custom_mail_error_content").append('<li>' + dollarCheck[i] + '</li>');
          }          
          $('#custom_mail_error_content li').wrapAll("<ul></ul>");
          
          $('#edit-submit').attr("value", "fix errors").attr("disabled", true);
          $('#edit-message').addClass("error");
          
          $('#edit-message').focus(function(){
            $('#edit-submit').attr("value", "Send message").attr("disabled", false);
            $('#edit-message').removeClass("error");
          });
          
          event.preventDefault();
          
        } else {
          //do nothing, message sent
        }
        
        
      });
    }
  };
})(jQuery);